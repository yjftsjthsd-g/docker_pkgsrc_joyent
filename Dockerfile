FROM rockylinux:9 AS base
LABEL maintainer "Brian Cole <docker@brianecole.com>"


# Copy and paste the lines below to install the 64-bit EL 7.x set.
ENV BOOTSTRAP_TAR "bootstrap-el7-trunk-x86_64-20220718.tar.gz"
ENV BOOTSTRAP_SHA "7ecea7fd3b49259aa7318d8cd72901966ad5329f"

WORKDIR /opt
COPY bootstrap-pkgsrc-joyent-el7.bash /opt/
RUN ["/bin/bash", "/opt/bootstrap-pkgsrc-joyent-el7.bash"]


# TODO: probably should stick this in /etc/profile or something
ENV PATH /usr/pkg/sbin:/usr/pkg/bin:$PATH
ENV MANPATH /usr/pkg/man:$MANPATH

CMD ["/bin/bash"]
