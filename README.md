# Docker image with pkgsrc from joyent

Sticks pkgsrc (from Joyent, in this case) into a CentOS 7 image.


## Use

```
sudo docker pull registry.gitlab.com/yjftsjthsd-g/docker_pkgsrc_joyent
sudo docker run --rm -it registry.gitlab.com/yjftsjthsd-g/docker_pkgsrc_joyent
command -v vim  # no vim
pkgin update  # optional; pkgin will apparently check on its own
pkgin search vim
pkgin install -y vim
command -v vim  # tada
```

