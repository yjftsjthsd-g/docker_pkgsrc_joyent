#!/bin/bash

# This is ripped from https://pkgsrc.joyent.com/install-on-linux/
# Changes:
# * I've taken all variables out since Docker will manage them
# * I've taken out `sudo` since Docker invokes this file as root


# Download the bootstrap kit to the current directory.
curl -O https://pkgsrc.smartos.org/packages/Linux/bootstrap/${BOOTSTRAP_TAR}

# Verify the SHA1 checksum.
echo "${BOOTSTRAP_SHA}  ${BOOTSTRAP_TAR}" >check-shasum
sha1sum -c check-shasum

# Verify PGP signature.  This step is optional, and requires gpg.
curl -O https://pkgsrc.joyent.com/packages/Linux/el7/bootstrap/${BOOTSTRAP_TAR}.asc
curl -sS https://pkgsrc.joyent.com/pgp/56AAACAF.asc | gpg --import
gpg --verify ${BOOTSTRAP_TAR}{.asc,}

# Install bootstrap kit to /usr/pkg
tar -xpf ${BOOTSTRAP_TAR} -C /
